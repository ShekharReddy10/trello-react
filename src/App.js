import { BrowserRouter, Route, Switch } from "react-router-dom";
import BoardItems from "./Components/BoardItems";
import MainContainer from "./Components/MainContainer";
import NavBar from "./Components/NavBar"
import SideBar from "./Components/SideBar";
import "./Components/Styles.css"

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <div className="container-fluid">
          <div className="rows">
            <div className="col-md-12 nav-bar ">
              <NavBar />
            </div>
          </div>
          <div className="rows full-container">
            <div className="col-2 sidebar">
              <SideBar />
            </div>
            <div className="col-10 main-container" >
              <Switch>
                <Route exact path="/" component={MainContainer}/>
                <Route path="/:id" component={BoardItems} />
              </Switch>
            </div>
          </div>
        </div>
      </div>
    </BrowserRouter>
  );
}

export default App;
