import React, { Component } from 'react'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEllipsis } from '@fortawesome/free-solid-svg-icons'
import Card from './Card'

import { createCard, deleteCard, getCard } from './Api'

class ListItems extends Component {
    constructor(props) {
        super(props)

        this.state = {
            cards: [],
            errorMsg: "",
            hasLoader: true,
            cardName: ""
        }
    }
    componentDidMount() {
        const fetchCards = async() => {
            try{
                const response = await getCard(this.props.list.id)
                this.setState({ cards: response.data})
            } catch{
                this.setState({ errorMsg: "Error while fetching cards" })
            }
            this.setState({hasLoader: false})
        }
        fetchCards()
    }
    handleCradName = (event) => {
        this.setState({ cardName: event.target.value })
    }
    addCard = (event) => {
        event.preventDefault()
        const fetchCards = async() => {
            this.setState({ hasLoader: true })
            try{
                const newCard = await createCard(this.props.list.id, event.target.name.value)
                this.setState({ cards: [...this.state.cards, newCard.data]})
            } catch {
                this.setState({ errorMsg: "Error while creating a card"})
            }
            this.setState({hasLoader: false, cardName: ""})
        }
       fetchCards()
    }
    deleteCard = (cardToBeDeleted) => {
        const fetchCards = async() => {
            this.setState({ hasLoader: true })
            try{
                await deleteCard(cardToBeDeleted.id)
                this.setState({ cards: this.state.cards.filter(card => card.id !== cardToBeDeleted.id)})
            } catch {
                this.setState({ errorMsg: "Error while deleting a card"})
            }
            this.setState({hasLoader: false})
        }
        fetchCards()
    }

    render() {
        const { cards, errorMsg, hasLoader } = this.state
        return (
            <div className='list-container' style={{ width: "20vw", height: "max-content" }}>
                <div className='card-header'>
                    <h5>{this.props.list.name}</h5>
                    <FontAwesomeIcon className='list-icon' icon={faEllipsis} />
                </div>
                {
                    hasLoader && <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                }
                {
                    cards.length ?
                        cards.map((card) => {
                            return <Card key={card.id} card={card} onDelete={this.deleteCard} />
                        }) : null
                }
                {
                    errorMsg ? <div>{errorMsg}</div> : null
                }
                <div className='add-a-card'>
                    <form onSubmit={this.addCard}>
                        <input type="text" name='name' placeholder='Add a card' className='card-text' required value={this.state.cardName} onChange={this.handleCradName} />
                        <button type='submit' className='btn btn-primary btn-sm'>Add Card</button>
                    </form>
                    <div className='btn btn-danger btn-sm delete-list' onClick={() => { this.props.onArchive(this.props.list) }}>Archive List</div>

                </div>
            </div>
        )
    }
}

export default ListItems