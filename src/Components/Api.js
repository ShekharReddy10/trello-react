import axios from 'axios'

axios.defaults.baseURL =  "https://api.trello.com/1/"
// const TOKEN = "ATTA0c5bb0805dd939fa8f8b7f39d73ff801e4b9162fa54722f308df5c2765e38ddf23B2A96D"
// const KEY = "f7e048e02ccc08f706c2ee962a1ee582"
axios.defaults.params = {
  key : "f7e048e02ccc08f706c2ee962a1ee582",
  token : "ATTA0c5bb0805dd939fa8f8b7f39d73ff801e4b9162fa54722f308df5c2765e38ddf23B2A96D"
}
// axios.defaults.params['key'] = "f7e048e02ccc08f706c2ee962a1ee582"
// axios.defaults.params['token'] = "ATTA0c5bb0805dd939fa8f8b7f39d73ff801e4b9162fa54722f308df5c2765e38ddf23B2A96D"

function getBoards() {
  return axios.get(`members/me/boards`)
}
function createBoard(boardName) {
  return axios.post(`boards/?name=${boardName}`)
}
function deleteBoard(boardId) {
  return axios.delete(`boards/${boardId}`)
}
function getList(boardId) {
  return axios.get(`boards/${boardId}/lists/`)
}
function createList(boardId, name) {
  return axios.post(`boards/${boardId}/lists?name=${name}`)
}
function archiveList(listId) {
  return axios.put(`lists/${listId}/?closed=true`)
}
function getCard(listId) {
  return axios.get(`lists/${listId}/cards`)
}
function createCard(listId, name) {
  return axios.post(`cards?idList=${listId}&name=${name}`)
}
function deleteCard(cardId) {
  return axios.delete(`cards/${cardId}`)
}

export {getBoards, createBoard, deleteBoard, createCard,createList, deleteCard, archiveList, getCard, getList}