import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import "./Styles.css"
import "bootstrap/dist/css/bootstrap.min.css"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faGripHorizontal, faSearch, faBell, faCircleExclamation } from '@fortawesome/free-solid-svg-icons'


function NavBar() {
  return (
    <Navbar expand="sm" className='Navbar' >
      <Container fluid>
        <div className='container-section'>
        <Navbar.Brand className='menu-icon' href="#home"><FontAwesomeIcon icon={faGripHorizontal} /></Navbar.Brand>
        <Nav >
          <NavDropdown title="Workspace" id="basic-nav-dropdown" >
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
          </NavDropdown>
          <NavDropdown title="Recent" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
          </NavDropdown>
          <NavDropdown title="Starred" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
          </NavDropdown>
          <NavDropdown title="Templates" id="basic-nav-dropdown">
            <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
          </NavDropdown>
          <Button size='sm' >Create</Button>
        </Nav>
        </div>
        <div className='container-section'>
        <Navbar.Brand className='menu-icon search' href="#home"><FontAwesomeIcon icon={faSearch} /></Navbar.Brand>

        <Form className="d-flex">
            <Form.Control
              type="search"
              title='Search'
              className="me-2"
              aria-label="Search"
            />
          </Form>
          <Navbar.Brand className='menu-icon' href="#home"><FontAwesomeIcon icon={faBell} /></Navbar.Brand>
          <Navbar.Brand className='menu-icon' href="#home"><FontAwesomeIcon icon={faCircleExclamation} /></Navbar.Brand>

          </div>
      </Container>
    </Navbar>
  )
}

export default NavBar