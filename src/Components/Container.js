import React from 'react'
import NavBar from './NavBar'
import SideBar from './SideBar'

function Container() {
    return (
        <div>
            <div><NavBar /></div>
            <div><SideBar /></div>
        </div>
    )
}

export default Container