import React, { Component } from 'react'
import Form from 'react-bootstrap/Form';
import Dropdown from 'react-bootstrap/Dropdown';
import Boards from './Boards';
import "./Styles.css"
export class MainContainer extends Component {
    render() {
        return (
            <div>
                <div className='container-header'>
                    <h2>Trello workspace</h2>
                </div>
                <div className='board-filters'>
                    <Dropdown className='board-sortby'>
                        <label>sort by</label>
                        <Dropdown.Toggle variant="light" id="dropdown-basic">
                        Most recent active
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                            <Dropdown.Item href="#/action-1">Most recent active</Dropdown.Item>
                            <Dropdown.Item href="#/action-2">Least recent active</Dropdown.Item>
                            <Dropdown.Item href="#/action-3">Alphabetically A-Z</Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>

                    <Dropdown  className='board-filterby'>
                    <label>Filter by</label>
                        <Dropdown.Toggle variant="light" id="dropdown-basic">
                            Choose a collection
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                            <Dropdown.Item href="#/action-1">Action</Dropdown.Item>

                        </Dropdown.Menu>
                    </Dropdown>
                    <div className='board-search'>
                        <label>Search</label>
                        <Form className="d-flex">
                            <Form.Control
                                type="search"
                                title='Search'
                                placeholder='Search boards'
                                className="me-2"
                                aria-label="Search"
                            />
                        </Form>
                    </div>
                </div>
                <div className='conatiner-boards'>
                    <Boards />
                </div>
            </div>
        )
    }
}

export default MainContainer