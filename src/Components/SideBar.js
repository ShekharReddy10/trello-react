import React, { Component } from 'react'
import './Styles.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTrello } from '@fortawesome/free-brands-svg-icons'
import { Link } from 'react-router-dom'

class SideBar extends Component {
  render() {
    return (
      <>
        <Link to={'/'} style={{textDecoration:"none"}}>
          <div className='side-bar-items'>
            <div className='side-bar-title'>
              <span >
                <FontAwesomeIcon icon={faTrello} />
              </span>
              Boards
            </div>
          </div>
        </Link>
      </>
    )
  }
}

export default SideBar