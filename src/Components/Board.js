import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import "./Styles.css"
class Board extends Component {
  render() {
    const { board } = this.props
    if(this.props.board.prefs)
    // const styles = {
    //   backgroundImage: `url("${this.props.board.prefs.backgroundImage}")`,
    //   backgroundPosition: "center center",
    //   backgroundSize: "cover",
    // }
    return (
      <div className='board ' >
        <Link to={`/${board.id}`} style={{ textDecoration: "none", width: "80%", height: "100%" }} > <div className='board-name'>{board.name}</div></Link><div className='btn btn-danger delete' onClick={() => { this.props.onDelete(board) }}><h4>Delete</h4></div>
      </div>
    )
  }
}

export default Board