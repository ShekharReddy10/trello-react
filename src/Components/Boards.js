import React, { Component } from 'react'

import Board from './Board'
import "./Styles.css"
import { getBoards, deleteBoard, createBoard } from './Api'

class Boards extends Component {
    constructor(props) {
        super(props)
        this.state = {
            boards: [],
            errorMsg: "",
            hasLoader: true,
            boardName: ""
        }
    }
    componentDidMount() {
        const fetchBoards = async () => {
            try {
                const response = await getBoards()
                console.log(response.data)
                this.setState({ boards: response.data, hasLoader: false })
            }
            catch {
                this.setState({ errorMsg: "Error while retrieving boards ", hasLoader: false })
            }
        }
        fetchBoards()
    }
    handleBoardName = (event) => {
        this.setState({ boardName: event.target.value })
    }
    createBoard = (event) => {
        event.preventDefault()
        const fetchBoards = async () => {
            this.setState({ hasLoader: true })
            try {
                const newBoard = await createBoard(event.target.name.value)
                this.setState({ boards: [...this.state.boards, newBoard.data], boardName: " " })
            } catch {
                this.setState({ errorMsg: "Error while deleting a boards ", boardName: "" })
            }
            this.setState({ hasLoader: false })
        }
        fetchBoards()
    }
    deleteBoard = (boardToBeDelete) => {
        console.log(boardToBeDelete.id)
        const fetchBoards = async () => {
            this.setState({ hasLoader: true })
            try {
                await deleteBoard(boardToBeDelete.id)
                this.setState({ boards: this.state.boards.filter(board => board.id !== boardToBeDelete.id) })
            } catch {
                this.setState({ errorMsg: "Unable to delete a board" })
            }
            this.setState({ hasLoader: false })
        }
        fetchBoards()
    }
    render() {
        const { errorMsg, boards, hasLoader } = this.state
        return (
            <React.Fragment>
                <div className='create-board container col-md-4 col-lg-4'>
                    <form onSubmit={this.createBoard}>
                        <label>Create a new board</label>
                        <input type="text" name='name' placeholder='Enter name of a board' className='board-text' required value={this.state.boardName} onChange={this.handleBoardName} />
                        <button type='submit' className='btn btn-primary add-board-button' >Add board</button>
                    </form>
                </div>
                {
                    hasLoader && <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
                }
                <div className='container boards' style={{ padding: "10px 0px" }}>
                    {
                        boards.length ?
                            boards.map((board) => {
                                return <Board key={board.id} board={board} onDelete={this.deleteBoard} />
                            }) : <h3>There are no boards. create a new board</h3>
                    }
                    {
                        errorMsg ? <div>{errorMsg}</div> : null
                    }
                </div>

            </React.Fragment>
        )
    }
}

export default Boards